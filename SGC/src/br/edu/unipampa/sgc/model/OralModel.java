/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.util.ArrayList;

/**
 *
 * @author Luciano
 */
public class OralModel extends Prova{
    private ArrayList<Apresentacao> apresentacoes;

    public OralModel(ArrayList<Apresentacao> apresentacoes) {
        this.apresentacoes = apresentacoes;
    }

    public OralModel(ArrayList<Apresentacao> apresentacoes, double peso, double nota, String result, ArrayList<Criterio> criterios) {
        super(peso, nota, result, criterios);
        this.apresentacoes = apresentacoes;
    }
    public OralModel() {
        super();
    }
    
    

    @Override
    public void gerarAtaJulgamento() {
        super.gerarAtaJulgamento(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaRealizacao() {
        super.gerarAtaRealizacao(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaResultado() {
        super.gerarAtaResultado(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarListaPresenca() {
        super.gerarListaPresenca(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarPlanilhaNotas() {
        super.gerarPlanilhaNotas(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the apresentacoes
     */
    public ArrayList<Apresentacao> getApresentacoes() {
        return apresentacoes;
    }

    /**
     * @param apresentacoes the apresentacoes to set
     */
    public void setApresentacoes(ArrayList<Apresentacao> apresentacoes) {
        this.apresentacoes = apresentacoes;
    }

  
}
