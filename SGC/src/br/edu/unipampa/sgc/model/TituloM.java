/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.io.Serializable;

/**
 *
 * @author Luciano
 */
public class TituloM implements Serializable{
    private String discriminacao;
    private double pontuacao;
    private int quantidade;
    private String classe;
    private String nome;
    
      public  TituloM(){
          
      }
      
     public TituloM(String discriminacao,double pontuacao,int qtd,String classe){
         this.discriminacao = discriminacao;
         this.classe = classe;
         this.pontuacao = pontuacao;
         this.quantidade = qtd;
         
     } 
    /**
     * @return the discriminacao
     */
    public String getDiscriminacao() {
        return discriminacao;
    }

    /**
     * @param discriminacao the discriminacao to set
     */
    public void setDiscriminacao(String discriminacao) {
        this.discriminacao = discriminacao;
    }

    /**
     * @return the pontuacao
     */
    public double getPontuacao() {
        return pontuacao;
    }

    /**
     * @param pontuacao the pontuacao to set
     */
    public void setPontuacao(double pontuacao) {
        this.pontuacao = pontuacao;
    }

    /**
     * @return the quantidade
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * @return the classe
     */
    public String getClasse() {
        return classe;
    }

    /**
     * @param classe the classe to set
     */
    public void setClasse(String classe) {
        this.classe = classe;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
}
