/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.util.ArrayList;

/**
 *
 * @author Luciano
 */
public class DidaticaModel extends Prova{
    private ArrayList<String> pontos;
    private ArrayList<String> sorteioCand;
    private String localDivulgacaoPonto;
    private ArrayList<Apresentacao> apresentacoes;
    private ArrayList<ListaRealizacao> lista;
    public DidaticaModel() {
        
        lista=new ArrayList<>();
    }

    public DidaticaModel(ArrayList<String> pontos, double peso, double nota, String result, ArrayList<Criterio> criterios) {
        super(peso, nota, result, criterios);
        this.pontos = pontos;
        lista=new ArrayList<>();
    }

    

    @Override
    public void gerarAtaJulgamento() {
        super.gerarAtaJulgamento(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaRealizacao() {
        super.gerarAtaRealizacao(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaResultado() {
        super.gerarAtaResultado(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarListaPresenca() {
        super.gerarListaPresenca(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarPlanilhaNotas() {
        super.gerarPlanilhaNotas(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the pontos
     */
    public ArrayList<String> getPontos() {
        return pontos;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(ArrayList<String> pontos) {
        this.pontos = pontos;
    }

    /**
     * @return the sorteioCand
     */
    public ArrayList<String> getSorteioCand() {
        return sorteioCand;
    }

    /**
     * @param sorteioCand the sorteioCand to set
     */
    public void setSorteioCand(ArrayList<String> sorteioCand) {
        this.sorteioCand = sorteioCand;
    }

    /**
     * @return the localDivulgacaoPonto
     */
    public String getLocalDivulgacaoPonto() {
        return localDivulgacaoPonto;
    }

    /**
     * @param localDivulgacaoPonto the localDivulgacaoPonto to set
     */
    public void setLocalDivulgacaoPonto(String localDivulgacaoPonto) {
        this.localDivulgacaoPonto = localDivulgacaoPonto;
    }

    /**
     * @return the apresentacoes
     */
    public ArrayList<Apresentacao> getApresentacoes() {
        return apresentacoes;
    }

    /**
     * @param apresentacoes the apresentacoes to set
     */
    public void setApresentacoes(ArrayList<Apresentacao> apresentacoes) {
        this.apresentacoes = apresentacoes;
    }

    /**
     * @return the lista
     */
    public ArrayList<ListaRealizacao> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(ArrayList<ListaRealizacao> lista) {
        this.lista = lista;
    }
    
    
}
