/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.util.ArrayList;

/**
 *
 * @author Luciano
 */
public class TitulosModel extends Prova{
    private ArrayList<TituloM> titulos;
    public TitulosModel() {
    }

    public TitulosModel(ArrayList<TituloM> titulos, double peso, double nota, String result, ArrayList<Criterio> criterios) {
        super(peso, nota, result, criterios);
        this.titulos = titulos;
    }

    
    public void calcularNotaTitulos(){
        
    }
    @Override
    public void gerarAtaJulgamento() {
        super.gerarAtaJulgamento(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaRealizacao() {
        super.gerarAtaRealizacao(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarListaPresenca() {
        super.gerarListaPresenca(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaResultado() {
        super.gerarAtaResultado(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarPlanilhaNotas() {
        super.gerarPlanilhaNotas(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the titulos
     */
    public ArrayList<TituloM> getTitulos() {
        return titulos;
    }

    /**
     * @param titulos the titulos to set
     */
    public void setTitulos(ArrayList<TituloM> titulos) {
        this.titulos = titulos;
    }
    
}
