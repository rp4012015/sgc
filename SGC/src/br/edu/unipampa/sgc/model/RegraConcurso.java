/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Luciano
 */
public class RegraConcurso implements Serializable{
    private int classeConcurso;
    private ArrayList<Prova> provas;

    public RegraConcurso() {
    }

    public RegraConcurso(int classeConcurso, ArrayList<Prova> provas) {
        this.classeConcurso = classeConcurso;
        this.provas = provas;
    }
    
        

    /**
     * @return the tipoConcurso
     */
    public int getClasseConcurso() {
        return classeConcurso;
    }

    /**
     * @param tipoConcurso the tipoConcurso to set
     */
    public void setTipoConcurso(int claseeConcurso) {
        this.classeConcurso = claseeConcurso;
    }

    /**
     * @return the provas
     */
    public ArrayList<Prova> getProvas() {
        return provas;
    }

    /**
     * @param provas the provas to set
     */
    public void setProvas(ArrayList<Prova> provas) {
        this.provas = provas;
    }
}
