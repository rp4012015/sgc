/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Luciano
 */
public class AberturaModel implements Serializable{
    private ArrayList<Atividade> atividades;
    private String local; 
    private Date horaInstalacao;
    private Date horaLeitura;
    private String portaria;
    private String emissorPortaria;

    public AberturaModel(){
        
    }
    public AberturaModel (ArrayList<Atividade> atividades){
        this.atividades = atividades;
    }

    /**
     * @return the atividades
     */
    public ArrayList<Atividade> getAtividades() {
        return atividades;
    }

    /**
     * @param atividades the atividades to set
     */
    public void setAtividades(ArrayList<Atividade> atividades) {
        this.atividades = atividades;
    }
    public void atribuirPesoProva (){
        
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the horaInstalacao
     */
    public Date getHora() {
        return horaInstalacao;
    }

    /**
     * @param hora the horaInstalacao to set
     */
    public void setHora(Date hora) {
        this.horaInstalacao = hora;
    }

    /**
     * @return the portaria
     */
    public String getPortaria() {
        return portaria;
    }

    /**
     * @param portaria the portaria to set
     */
    public void setPortaria(String portaria) {
        this.portaria = portaria;
    }

    /**
     * @return the emissorPortaria
     */
    public String getEmissorPortaria() {
        return emissorPortaria;
    }

    /**
     * @param emissorPortaria the emissorPortaria to set
     */
    public void setEmissorPortaria(String emissorPortaria) {
        this.emissorPortaria = emissorPortaria;
    }

    /**
     * @return the horaLeitura
     */
    public Date getHoraLeitura() {
        return horaLeitura;
    }

    /**
     * @param horaLeitura the horaLeitura to set
     */
    public void setHoraLeitura(Date horaLeitura) {
        this.horaLeitura = horaLeitura;
    }
}
