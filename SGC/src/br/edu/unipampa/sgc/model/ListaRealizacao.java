/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.util.Date;

/**
 *
 * @author Luciano
 */
public class ListaRealizacao {
    private CandidatoModel candidatoModel;
    private Date data;
    private String presenca;

    public ListaRealizacao(CandidatoModel candidatoModel, Date data, String presenca) {
        this.candidatoModel = candidatoModel;
        this.data = data;
        this.presenca = presenca;
    }

    public ListaRealizacao() {
    }

    

    /**
     * @return the candidatoModel
     */
    public CandidatoModel getCandidatoModel() {
        return candidatoModel;
    }

    /**
     * @param candidatoModel the candidatoModel to set
     */
    public void setCandidatoModel(CandidatoModel candidatoModel) {
        this.candidatoModel = candidatoModel;
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @return the presenca
     */
    public String getPresenca() {
        return presenca;
    }

    /**
     * @param presenca the presenca to set
     */
    public void setPresenca(String presenca) {
        this.presenca = presenca;
    }
    
}
