/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Luciano
 */
public class CandidatoModel extends Pessoa {

    private Date dataNascimento;
    private int etapa;
    private double[][] notas;
    private double notaFinal;
    
    private ArrayList<TituloM> titulos;

    public CandidatoModel() {
        super();
        this.etapa = 0;
        
        this.titulos = new ArrayList<TituloM>();
    }

    public CandidatoModel(String nome, String sexo, Date dataNasc,int examinadores) {
        super(nome, sexo);
        this.dataNascimento = dataNasc;
        this.etapa = 0;
        this.notas = new double[examinadores][4];
        this.titulos = new ArrayList<TituloM>();

    }

    /**
     * @return the dataNascimento
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * @param dataNascimento the dataNascimento to set
     */
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * @return the etapa
     */
    public int getEtapa() {
        return etapa;
    }

    /**
     * @param etapa the etapa to set
     */
    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    /**
     * @return the notas
     */
    public double[][] getNotas() {
        return notas;
    }
    public double getNotaParcial(int prova) {
       double notaParcial=0;
        for (int i = 0; i < notas.length; i++) {
            notaParcial= notaParcial+notas[i][prova];
        }
        notaParcial=notaParcial/notas.length;
        return notaParcial;
    }

    public double getNotas2(int examinador,int prova) {
        return notas[examinador][prova];
    }

    /**
     * @param notas the notas to set
     */
    public void setNotas(double[][] notas) {
        this.notas = notas;
    }

    public void setNotas2(double notas, int prova, int examinador) {
        this.notas[examinador][prova] = notas;
    }
    public void setNotas3(double notas, int prova) {
        for (int i = 0; i < this.notas.length; i++) {
        this.notas[i][prova] = notas;
        }
    }

    /**
     * @return the titulos
     */
    public ArrayList<TituloM> getTitulos() {
        return titulos;
    }

    /**
     * @param titulos the titulos to set
     */
    public void setTitulos(ArrayList<TituloM> titulos) {
        this.titulos = titulos;
    }

    /**
     * @return the notaFinal
     */
    public double getNotaFinal() {
        return notaFinal;
    }

    /**
     * @param notaFinal the notaFinal to set
     */
    public void setNotaFinal(double notaFinal) {
        this.notaFinal = notaFinal;
    }

}
