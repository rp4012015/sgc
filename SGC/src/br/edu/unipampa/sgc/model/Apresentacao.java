/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Luciano
 */
public class Apresentacao implements Serializable{
    private CandidatoModel candidatoModel;
    private Date data;
    private String presenca;
    private String tema;

    public Apresentacao() {
    }

    public Apresentacao(CandidatoModel candidatoModel, Date data, String presenca, String tema) {
        this.candidatoModel = candidatoModel;
        this.data = data;
        this.presenca = presenca;
        this.tema = tema;
    }

    
    /**
     * @return the candidatoModel
     */
    public CandidatoModel getCandidatoModel() {
        return candidatoModel;
    }

    /**
     * @param candidatoModel the candidatoModel to set
     */
    public void setCandidatoModel(CandidatoModel candidatoModel) {
        this.candidatoModel = candidatoModel;
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @return the presenca
     */
    public String getPresenca() {
        return presenca;
    }

    /**
     * @param presenca the presenca to set
     */
    public void setPresenca(String presenca) {
        this.presenca = presenca;
    }

    /**
     * @return the tema
     */
    public String getTema() {
        return tema;
    }

    /**
     * @param tema the tema to set
     */
    public void setTema(String tema) {
        this.tema = tema;
    }
    
}
