/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import br.edu.unipampa.sgc.DAO.Deserializar;
import br.edu.unipampa.sgc.DAO.GeradorDoc;
import br.edu.unipampa.sgc.DAO.Serializar;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Luciano
 */
public class ConcursoModel implements Serializable {

    private String universidade;
    private String nome;
    private String ministerio;
    private Date dataInicio;
    private ArrayList<CandidatoModel> candidatosAtivos;
    private ArrayList<CandidatoModel> candidatosEliminados;
    private AberturaModel abertura;
    private String area;
    private String campus;
    private String edital;
    private RegraConcurso regra;
    private String diretorio;
    private ArrayList<Examinador> examinadores;
    private Serializar salvar;
    private GeradorDoc doc;
    private Deserializar carregar;
    private int etapa;

    public ConcursoModel() {
        carregar = new Deserializar();
        salvar = new Serializar();
        
        doc = new GeradorDoc();
        etapa = 0;
    }

    public ConcursoModel(String universidade, String ministerio, String campus, String area, String edital, Date dataInicio) {
        this.universidade = universidade;
        this.ministerio = ministerio;
        this.dataInicio = dataInicio;
        etapa = 0;
        this.candidatosEliminados = new ArrayList<>();
        this.area = area;
        this.edital = edital;
        this.campus = campus;
        carregar = new Deserializar();
        salvar = new Serializar();
        doc = new GeradorDoc();
    }

    public ConcursoModel(String universidade, String ministerio, Date dataInicio, ArrayList<CandidatoModel> candidatos, AberturaModel abertura, RegraConcurso regra, String diretorio, ArrayList<Examinador> examinadores) {
        this.universidade = universidade;
        this.ministerio = ministerio;
        this.dataInicio = dataInicio;
        this.candidatosAtivos = candidatos;
        this.abertura = abertura;
        this.regra = regra;
        this.diretorio = diretorio;
        this.examinadores = examinadores;
        carregar = new Deserializar();
        salvar = new Serializar();
        etapa = 0;
    }

    public void salvarConcurso(ConcursoModel c) {
        String path = diretorio + "\\" + nome;

      
        try {
            getSalvar().serializar(path, c);
        } catch (Exception ex2) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex2);
        }
        

    }
    
    public void gerarAtaNaoImpedimento(){
        try {
            doc.gerarNaoimpedimento(diretorio,"NaoImpedimento.doc", campus, area,classeConcurso() , edital);
        } catch (IOException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void gerarDesistenciaEscrita(){
        try {
            doc.gerarDesistenciaEscrita(diretorio,"DesistenciaEscrita.doc", campus, area, classeConcurso(), edital);
            
        } catch (IOException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void gerarDesistenciaDidatica(){
        try {
            doc.gerarDesistenciaDidatica(diretorio,"DesistenciaDidatica.doc", campus, area, classeConcurso(), edital);
            
        } catch (IOException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void gerarPautaProva(){
        try {
            doc.gerarPautaProvaEscrita(diretorio,"PautaProvaEscrita.doc", campus, area, classeConcurso());
        } catch (IOException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void gerarReciboDoc(){
        try {
            doc.gerarReciboDocumento(diretorio,"ReciboDocumento.doc", campus, area, classeConcurso(), edital, examinadores.get(0).getNome());
        } catch (IOException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
    public String classeConcurso(){
        String r1="";
        if (regra.getClasseConcurso()==0) {
            r1 = "Titular Livre";
            
        }
        if (regra.getClasseConcurso()==1) {
            r1 = "Magistério Superior";
            
        }
        return r1;
    }
    public void gerarAta1() {
        
        try {
            doc.gerarAta(diretorio,"AtaAbertura.doc",campus,area,classeConcurso(),"Ata","1",examinadores.get(0).getNome(),examinadores.get(1).getNome(),examinadores.get(2).getNome());
        } catch (JAXBException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public ConcursoModel carregarConcursoModel(String dir) {
        ConcursoModel c = new ConcursoModel();
        try {
            c = (ConcursoModel) getCarregar().deserializar(dir);
        } catch (Exception ex) {
            Logger.getLogger(ConcursoModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        return c;
    }

    /**
     * @return the universidade
     */
    public String getUniversidade() {
        return universidade;
    }

    /**
     * @param universidade the universidade to set
     */
    public void setUniversidade(String universidade) {
        this.universidade = universidade;
    }

    /**
     * @return the ministerio
     */
    public String getMinisterio() {
        return ministerio;
    }

    /**
     * @param ministerio the ministerio to set
     */
    public void setMinisterio(String ministerio) {
        this.ministerio = ministerio;
    }

    /**
     * @return the dataInicio
     */
    public Date getDataInicio() {
        return dataInicio;
    }

    /**
     * @param dataInicio the dataInicio to set
     */
    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * @return the candidatosAtivos
     */
    public ArrayList<CandidatoModel> getCandidatos() {
        return candidatosAtivos;
    }

    /**
     * @param candidatos the candidatosAtivos to set
     */
    public void setCandidatos(ArrayList<CandidatoModel> candidatos) {
        this.candidatosAtivos = candidatos;
    }

    /**
     * @return the abertura
     */
    public AberturaModel getAbertura() {
        return abertura;
    }

    /**
     * @param abertura the abertura to set
     */
    public void setAbertura(AberturaModel abertura) {
        this.abertura = abertura;
    }

    /**
     * @return the regra
     */
    public RegraConcurso getRegra() {
        return regra;
    }

    /**
     * @param regra the regra to set
     */
    public void setRegra(RegraConcurso regra) {
        this.regra = regra;
    }

    /**
     * @return the diretorio
     */
    public String getDiretorio() {
        return diretorio;
    }

    /**
     * @param diretorio the diretorio to set
     */
    public void setDiretorio(String diretorio) {
        this.diretorio = diretorio;
    }

    /**
     * @return the examinadores
     */
    public ArrayList<Examinador> getExaminadores() {
        return examinadores;
    }

    /**
     * @param examinadores the examinadores to set
     */
    public void setExaminadores(ArrayList<Examinador> examinadores) {
        this.examinadores = examinadores;
    }

    /**
     * @return the campus
     */
    public String getCampus() {
        return campus;
    }

    /**
     * @param campus the campus to set
     */
    public void setCampus(String campus) {
        this.campus = campus;
    }

    /**
     * @return the edital
     */
    public String getEdital() {
        return edital;
    }

    /**
     * @param edital the edital to set
     */
    public void setEdital(String edital) {
        this.edital = edital;
    }

    /**
     * @return the area
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * @return the candidatosEliminados
     */
    public ArrayList<CandidatoModel> getCandidatosEliminados() {
        return candidatosEliminados;
    }

    /**
     * @param candidatosEliminados the candidatosEliminados to set
     */
    public void setCandidatosEliminados(ArrayList<CandidatoModel> candidatosEliminados) {
        this.candidatosEliminados = candidatosEliminados;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the salvar
     */
    public Serializar getSalvar() {
        return salvar;
    }

    /**
     * @param salvar the salvar to set
     */
    public void setSalvar(Serializar salvar) {
        this.salvar = salvar;
    }

    /**
     * @return the carregar
     */
    public Deserializar getCarregar() {
        return carregar;
    }

    /**
     * @param carregar the carregar to set
     */
    public void setCarregar(Deserializar carregar) {
        this.carregar = carregar;
    }

    /**
     * @return the etapa
     */
    public int getEtapa() {
        return etapa;
    }

    /**
     * @param etapa the etapa to set
     */
    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

}
