/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Luciano
 */
public class EscritaModel extends Prova{
    private ArrayList<String> pontos;
    private Date horaSorteio;
    private Date horaInicio;
    private Date horaEncerra;
    private Date horaJulgamento;
    private String localJulgamento;
    private int pontoSorteado;
    public EscritaModel(){
        super();
    }

    public EscritaModel(ArrayList<String> pontos, double peso, double nota, String result, ArrayList<Criterio> criterios) {
        super(peso, nota, result, criterios);
        this.pontos = pontos;
    }

   
    
    
    
    public void gerarAtaLeitura(){
        
        
    }

    @Override
    public void gerarAtaJulgamento() {
        super.gerarAtaJulgamento(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaRealizacao() {
        super.gerarAtaRealizacao(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaResultado() {
        super.gerarAtaResultado(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarListaPresenca() {
        super.gerarListaPresenca(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarPlanilhaNotas() {
        super.gerarPlanilhaNotas(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the pontos
     */
    public ArrayList<String> getPontos() {
        return pontos;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(ArrayList<String> pontos) {
        this.pontos = pontos;
    }

    /**
     * @return the horaSorteio
     */
    public Date getHoraSorteio() {
        return horaSorteio;
    }

    /**
     * @param horaSorteio the horaSorteio to set
     */
    public void setHoraSorteio(Date horaSorteio) {
        this.horaSorteio = horaSorteio;
    }

    /**
     * @return the horaInicio
     */
    public Date getHoraInicio() {
        return horaInicio;
    }

    /**
     * @param horaInicio the horaInicio to set
     */
    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * @return the horaEncerra
     */
    public Date getHoraEncerra() {
        return horaEncerra;
    }

    /**
     * @param horaEncerra the horaEncerra to set
     */
    public void setHoraEncerra(Date horaEncerra) {
        this.horaEncerra = horaEncerra;
    }

    /**
     * @return the pontoSorteado
     */
    public int getPontoSorteado() {
        return pontoSorteado;
    }

    /**
     * @param pontoSorteado the pontoSorteado to set
     */
    public void setPontoSorteado(int pontoSorteado) {
        this.pontoSorteado = pontoSorteado;
    }

    /**
     * @return the horaJulgamento
     */
    public Date getHoraJulgamento() {
        return horaJulgamento;
    }

    /**
     * @param horaJulgamento the horaJulgamento to set
     */
    public void setHoraJulgamento(Date horaJulgamento) {
        this.horaJulgamento = horaJulgamento;
    }

    /**
     * @return the localJulgamento
     */
    public String getLocalJulgamento() {
        return localJulgamento;
    }

    /**
     * @param localJulgamento the localJulgamento to set
     */
    public void setLocalJulgamento(String localJulgamento) {
        this.localJulgamento = localJulgamento;
    }
    
}
