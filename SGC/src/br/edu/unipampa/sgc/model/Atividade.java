/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Luciano
 */
public class Atividade implements Serializable{
 private Date data;
 private String local;
 private String nome;
 
 
    public Atividade(){
        
    }
    public Atividade(String nome,String local,Date data){
        this.data = data;
        this.local = local;
        this.nome = nome;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }
}
