/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author Luciano
 */
public abstract class Prova implements Serializable{
    private double peso;
    
    private String divulgacaoResultado;
    private String localProva;
    private Date dataProva;
    private Date dataDivulgacaoResultado;
    private ArrayList<Criterio> criterios;
    
    public Prova(){
        
        
    }
    
    public Prova(double peso,double nota,String result,ArrayList<Criterio> criterios){
        this.peso = peso;
        this.divulgacaoResultado = result;
        this.criterios = criterios;
    }
    /**
     * @return the peso
     */
    public double getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(double peso) {
        this.peso = peso;
    }

    


    /**
     * @return the divulgacaoResultado
     */
    public String getDivulgacaoResultado() {
        return divulgacaoResultado;
    }

    /**
     * @param divulgacaoResultado the divulgacaoResultado to set
     */
    public void setDivulgacaoResultado(String divulgacaoResultado) {
        this.divulgacaoResultado = divulgacaoResultado;
    }
   public void gerarAtaJulgamento(){
       
   } 
   public void gerarListaPresenca(){
       
   } 
   public void gerarAtaRealizacao(){
       
   } 
   public void gerarAtaResultado(){
       
   } 
   public void gerarPlanilhaNotas(){
       
   } 

    /**
     * @return the criterios
     */
    public ArrayList<Criterio> getCriterios() {
        return criterios;
    }

    /**
     * @param criterios the criterios to set
     */
    public void setCriterios(ArrayList<Criterio> criterios) {
        this.criterios = criterios;
    }

    /**
     * @return the localProva
     */
    public String getLocalProva() {
        return localProva;
    }

    /**
     * @param localProva the localProva to set
     */
    public void setLocalProva(String localProva) {
        this.localProva = localProva;
    }

    /**
     * @return the dataProva
     */
    public Date getDataProva() {
        return dataProva;
    }

    /**
     * @param dataProva the dataProva to set
     */
    public void setDataProva(Date dataProva) {
        this.dataProva = dataProva;
    }

    /**
     * @return the dataDivulgacaoResultado
     */
    public Date getDataDivulgacaoResultado() {
        return dataDivulgacaoResultado;
    }

    /**
     * @param dataDivulgacaoResultado the dataDivulgacaoResultado to set
     */
    public void setDataDivulgacaoResultado(Date dataDivulgacaoResultado) {
        this.dataDivulgacaoResultado = dataDivulgacaoResultado;
    }

  

   

   
   
}
