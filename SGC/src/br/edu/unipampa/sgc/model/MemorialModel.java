/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.sgc.model;

import java.util.ArrayList;

/**
 *
 * @author Luciano
 */
public class MemorialModel extends Prova{
    private ArrayList<ListaRealizacao> lista;
    public MemorialModel() {
        
        lista = new ArrayList<ListaRealizacao>();
    }

    public MemorialModel(double peso, double nota, String result, ArrayList<Criterio> criterios) {
        super(peso, nota, result, criterios);
        lista = new ArrayList<ListaRealizacao>();
    }

   

    @Override
    public void gerarAtaJulgamento() {
        super.gerarAtaJulgamento(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaRealizacao() {
        super.gerarAtaRealizacao(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarAtaResultado() {
        super.gerarAtaResultado(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarListaPresenca() {
        super.gerarListaPresenca(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void gerarPlanilhaNotas() {
        super.gerarPlanilhaNotas(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the lista
     */
    public ArrayList<ListaRealizacao> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(ArrayList<ListaRealizacao> lista) {
        this.lista = lista;
    }
    
}
