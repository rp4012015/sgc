
package br.edu.unipampa.sgc.DAO;

import java.io.*;

/**
 *
 * @author fernandolima
 */
public class Serializar implements Serializable {

    public void serializar(String path, Object obj) throws Exception{
        
        FileOutputStream outFile = new FileOutputStream(path);
        ObjectOutputStream s = new ObjectOutputStream(outFile);
        s.writeObject(obj);
        s.close();

    }
    /* tem que criar os try e catch, e ja implementei o método com entrada do 
     * PATH e o proprio OBJETO
    **/
    
     }
