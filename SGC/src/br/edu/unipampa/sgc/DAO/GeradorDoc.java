package br.edu.unipampa.sgc.DAO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import javax.xml.bind.JAXBException;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Section;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.util.Date;

public class GeradorDoc implements Serializable {

    private static HWPFDocument replaceText(HWPFDocument doc, String findText, String replaceText) {
        Range r1 = doc.getRange();

        for (int i = 0; i < r1.numSections(); ++i) {
            Section s = r1.getSection(i);
            for (int x = 0; x < s.numParagraphs(); x++) {
                Paragraph p = s.getParagraph(x);
                for (int z = 0; z < p.numCharacterRuns(); z++) {
                    CharacterRun run = p.getCharacterRun(z);
                    String text = run.text();
                    if (text.contains(findText)) {
                        run.replaceText(findText, replaceText);
                    }
                }
            }
        }
        return doc;
    }

    private static void saveWord(String filePath, HWPFDocument doc) throws FileNotFoundException, IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filePath);
            doc.write(out);
        } finally {
            out.close();
        }
    }

    public static int getDia() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static String getMes() {
        Calendar c = Calendar.getInstance();
        String[] meses = {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
            "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
        System.out.println(c.get(Calendar.MONTH));
        return meses[c.get(Calendar.MONTH)];
    }

    public static int getAno() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR);

    }

    public void gerarAta(String diretorio, String filePath, String campus, String area, String regra, String nomeAta,
            String numAta, String banca1, String banca2, String banca3) throws IOException, JAXBException {

        POIFSFileSystem fs = null;

        try {
            fs = new POIFSFileSystem(new FileInputStream(filePath));
            HWPFDocument doc = new HWPFDocument(fs);

            //cabeçalho e corpo
            doc = replaceText(doc, "$campus", campus);
            doc = replaceText(doc, "$area", area);
            doc = replaceText(doc, "$regra", regra);

            //identificando documento
            doc = replaceText(doc, "$ata", nomeAta);
            doc = replaceText(doc, "$numAta", numAta);

            //data
            doc = replaceText(doc, "$data", getDia() + " de " + getMes() + " de " + getAno());
            doc = replaceText(doc, "$mes", getMes());

            //banca
            doc = replaceText(doc, "$banca1", banca1);
            doc = replaceText(doc, "$banca2", banca2);
            doc = replaceText(doc, "$banca3", banca3);

            saveWord(diretorio + "/" + filePath, doc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Gera ata de declaração de não impedimento
    public void gerarNaoimpedimento(String diretorio, String filePath, String campus, String area, String regra, String edital) throws IOException, JAXBException {

        POIFSFileSystem fs = null;

        try {
            fs = new POIFSFileSystem(new FileInputStream(filePath));
            HWPFDocument doc = new HWPFDocument(fs);

            //cabeçalho e corpo
            doc = replaceText(doc, "$campus", campus);
            doc = replaceText(doc, "$area", area);
            doc = replaceText(doc, "$regra", regra);

            //identificando documento
            doc = replaceText(doc, "$edital", edital);

            //data
            doc = replaceText(doc, "$data", getDia() + " de " + getMes() + " de " + getAno());

            saveWord(diretorio + "/" + filePath, doc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Gera ata de Desistencia da prova escrita
    public void gerarDesistenciaEscrita(String diretorio, String filePath, String campus, String area, String regra, String edital) throws IOException, JAXBException {

        POIFSFileSystem fs = null;

        try {
            fs = new POIFSFileSystem(new FileInputStream(filePath));
            HWPFDocument doc = new HWPFDocument(fs);

            //cabeçalho e corpo
            doc = replaceText(doc, "$campus", campus);
            doc = replaceText(doc, "$area", area);
            doc = replaceText(doc, "$regra", regra);

            //identificando documento
            doc = replaceText(doc, "$edital", edital);

            //data
            doc = replaceText(doc, "$data", getDia() + " de " + getMes() + " de " + getAno());

            saveWord(diretorio + "/" + filePath, doc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Gera ata de Desistencia da prova didatica
    public void gerarDesistenciaDidatica(String diretorio, String filePath, String campus, String area, String regra, String edital) throws IOException, JAXBException {

        POIFSFileSystem fs = null;

        try {
            fs = new POIFSFileSystem(new FileInputStream(filePath));
            HWPFDocument doc = new HWPFDocument(fs);

            //cabeçalho e corpo
            doc = replaceText(doc, "$campus", campus);
            doc = replaceText(doc, "$area", area);
            doc = replaceText(doc, "$regra", regra);

            //identificando documento
            doc = replaceText(doc, "$edital", edital);

            //data
            doc = replaceText(doc, "$data", getDia() + " de " + getMes() + " de " + getAno());

            //banca
            saveWord(diretorio + "/" + filePath, doc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Gera recibo de documentação
    public void gerarReciboDocumento(String diretorio, String filePath, String campus, String area, String regra, String edital, String banca1) throws IOException, JAXBException {

        POIFSFileSystem fs = null;

        try {
            fs = new POIFSFileSystem(new FileInputStream(filePath));
            HWPFDocument doc = new HWPFDocument(fs);

            //cabeçalho e corpo
            doc = replaceText(doc, "$campus", campus);
            doc = replaceText(doc, "$area", area);
            doc = replaceText(doc, "$regra", regra);

            //identificando documento
            doc = replaceText(doc, "$edital", edital);

            //data
            doc = replaceText(doc, "$data", getDia() + " de " + getMes() + " de " + getAno());

            //banca
            doc = replaceText(doc, "$banca1", banca1);

            saveWord(diretorio + "/" + filePath, doc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Gera folha para pauta da prova escrita
    public void gerarPautaProvaEscrita(String diretorio, String filePath, String campus, String area, String regra) throws IOException, JAXBException {

        POIFSFileSystem fs = null;

        try {
            fs = new POIFSFileSystem(new FileInputStream(filePath));
            HWPFDocument doc = new HWPFDocument(fs);

            //cabeçalho e corpo
            doc = replaceText(doc, "$campus", campus);
            doc = replaceText(doc, "$area", area);
            doc = replaceText(doc, "$regra", regra);

            saveWord(diretorio + "/" + filePath, doc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
