
package br.edu.unipampa.sgc.DAO;

import java.io.*;

/**
 *
 * @author fernandolima
 */
public class Deserializar implements Serializable{
    public Object deserializar(String dir) throws Exception {
          String path = dir;
    FileInputStream inFile = new FileInputStream(path);
    ObjectInputStream d = new ObjectInputStream(inFile);
    Object o = d.readObject();
    d.close();
    return o;
    }
}
