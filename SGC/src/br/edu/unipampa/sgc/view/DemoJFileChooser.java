package br.edu.unipampa.sgc.view;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.File;
import java.text.DecimalFormat;
import java.util.*;

public class DemoJFileChooser extends JPanel
        implements ActionListener {

    private Configuracao c1;
    private Abrir a1;
    private JFileChooser chooser;
    private String choosertitle;
    int opt;

    public DemoJFileChooser(Configuracao c, int i) {
        c1 = c;
        opt = i;
    }

    public DemoJFileChooser(Abrir a, int i) {
        a1 = a;
        opt = i;
    }

    public void actionPerformed(ActionEvent e) {
        int result;

        setChooser(new JFileChooser());
        getChooser().setCurrentDirectory(new java.io.File("."));
        getChooser().setDialogTitle(getChoosertitle());
        if (opt == 0) {
        getChooser().setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }
       
    //
        // disable the "All files" option.
        //
        getChooser().setAcceptAllFileFilterUsed(false);
        //    
        if (getChooser().showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath().replace("\\", "\\\\");
            if (opt == 0) {

                
                c1.getjTextField7().setText(path);
            }
            if (opt == 1) {

                
                a1.getjTextField1().setText(path);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nada Foi Selecionado");
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension(200, 200);
    }

    /**
     * @return the chooser
     */
    public JFileChooser getChooser() {
        return chooser;
    }

    /**
     * @param chooser the chooser to set
     */
    public void setChooser(JFileChooser chooser) {
        this.chooser = chooser;
    }

    /**
     * @return the c1
     */
    public Configuracao getC1() {
        return c1;
    }

    /**
     * @param c1 the c1 to set
     */
    public void setC1(Configuracao c1) {
        this.c1 = c1;
    }

    /**
     * @return the choosertitle
     */
    public String getChoosertitle() {
        return choosertitle;
    }

    /**
     * @param choosertitle the choosertitle to set
     */
    public void setChoosertitle(String choosertitle) {
        this.choosertitle = choosertitle;
    }

}
